variable "region" {
  type        = string
  description = "AWS region"
}

variable "datadog_monitors" {
  type        = map(map(map(any)))
  description = "Map of Datadog monitor configs"
}
