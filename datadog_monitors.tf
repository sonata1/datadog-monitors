resource "datadog_monitor" "monitor" {
  for_each = var.datadog_monitors

  name                     = each.value.parameters.name
  type                     = each.value.parameters.type
  query                    = each.value.parameters.query
  message                  = each.value.parameters.message
  escalation_message       = lookup(each.value.parameters, "escalation_message", null)
  require_full_window      = lookup(each.value.parameters, "require_full_window", null)
  notify_no_data           = lookup(each.value.parameters, "notify_no_data", null)
  new_group_delay          = lookup(each.value.parameters, "new_group_delay", null)
  evaluation_delay         = lookup(each.value.parameters, "evaluation_delay", null)
  no_data_timeframe        = lookup(each.value.parameters, "no_data_timeframe", null)
  renotify_interval        = lookup(each.value.parameters, "renotify_interval", null)
  notify_audit             = lookup(each.value.parameters, "notify_audit", null)
  timeout_h                = lookup(each.value.parameters, "timeout_h", null)
  include_tags             = lookup(each.value.parameters, "include_tags", null)
  enable_logs_sample       = lookup(each.value.parameters, "enable_logs_sample", null)
  force_delete             = lookup(each.value.parameters, "force_delete", null)
  tags                     = flatten(concat([for key, value in lookup(each.value, "tags", {}) : "${key}:${value}"], [for key, value in lookup(each.value, "custom_tags", {}) : "${value}"]))
  priority                 = lookup(each.value.parameters, "priority", null)
  renotify_statuses        = [for key, value in lookup(each.value, "renotify_statuses", {}) : "${value}"]
  on_missing_data          = lookup(each.value.parameters, "on_missing_data", null)
  notification_preset_name = lookup(each.value.parameters, "notification_preset_name", null)

  monitor_thresholds {
    warning           = lookup(lookup(each.value, "thresholds", {}), "warning", null)
    warning_recovery  = lookup(lookup(each.value, "thresholds", {}), "warning_recovery", null)
    critical          = lookup(lookup(each.value, "thresholds", {}), "critical", null)
    critical_recovery = lookup(lookup(each.value, "thresholds", {}), "critical_recovery", null)
    ok                = lookup(lookup(each.value, "thresholds", {}), "ok", null)
    unknown           = lookup(lookup(each.value, "thresholds", {}), "unknown", null)
  }

  monitor_threshold_windows {
    recovery_window = lookup(lookup(each.value, "threshold_windows", {}), "recovery_window", null)
    trigger_window  = lookup(lookup(each.value, "threshold_windows", {}), "trigger_window", null)
  }
}
